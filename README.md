#  Evilock NFC Private Messenger Server
### evilock-nfc-messaging

> This project is a lightweight open-source messaging server where the message packets are sent with a timer. A periodical script removes all the pending messages when their timers reach 0.

## Prerequisites

* Download and install docker and docker-compose:
https://www.docker.com/community-edition#/download
* Download and install Node.js:
https://nodejs.org/es/download/

## Running
* git clone https://bitbucket.org/freemindtronic/evilock-nfc-private-messenger-server.git
* ./run.sh or ./run.bat

## Usage
Inside docker container:

* Start the messaging server : `npm run start`
* Start the test client : `npm run test`
* Remove all database entries : `node cleandb`

## View logs

* docker exec -ti evilock-nfc-messaging /bin/bash
* tail -1000f /var/log/messaging-server/messagingServer.log