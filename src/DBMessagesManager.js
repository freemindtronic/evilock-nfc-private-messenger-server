const __ = require('./Constants.js');
const _ = require('lodash');
const MongoClient = require('mongodb').MongoClient

class DBMessagesManager {
	constructor(db) {
		this.db = db;
		this.messages = this.db.collection('messages'); //Name of the collection where messages are saved
		console.log("Database is connected...");
	}

	//Add the current message to a document
	addMessage(message) {
		var messageAndDate = _.split(message[2], __.SEPARATOR_DATE);
		  this.messages.insertOne(
		  	{
		  		uid_tag_sender: message[0], 
		  		uid_tag_receiver: message[1], 
		  		message_body: messageAndDate[0],
				message_date: messageAndDate[1],
		  		timer: parseInt(message[3], 10)
		  	}, 
		  	function(err, r) //Callback after insert
		  	{
		  		if (null != err || r.insertedCount != 1)
		  			console.error("Insert message failed !");

			    //We get back every message for a given UID
			    var cursor = this.messages.find({
			    	uid_tag_receiver: message[1]
			    });

			    var count = 0;
			    cursor.forEach(function(doc) {
			        console.log("Current user message timer : " + doc.timer);
			        count = count + 1;
			    }, function(err) {
			    	if (err != null)
			    		throw err;
			    	else
			        	console.log("Database : Message successfully added...");
			    });
			}.bind(this));
		};

		//Decreases every message timer by 1
		updateTimers() {	
			this.messages.updateMany({}, {$inc:{"timer":-1}}, { $type: "number" }, function(err, r) 
			{
				if (err != null)
					console.error(err);
				else {
					console.log("UPDATE : " + r.result.n + " timers have been updated");
					this.removeExpiredMessages();
				}
			}.bind(this));
		}

		//Remove every message when the timer reaches 0
		removeExpiredMessages() {
			this.messages.removeMany({timer: 0}, function(err, r) 
			{
				if (err != null)
					console.error(err);
				else
					console.log("DELETE : " + r.result.n + " messages have been deleted");
			});
		}

		//Retrieves the pending messages of the 'uid_receiver' and returns an array sorted by timer (the latter messages will come first)
		getReceivedMessages(uid_receiver) {
			var messages_collection = this.messages;
			if (!(typeof uid_receiver === 'string'))
				return -1;

			else return new Promise(function(resolve,reject) 
			{
				var cursor = messages_collection.find({
		    		uid_tag_receiver: uid_receiver
			    });

			    var count = 0;
			    var array_messages = [];
			    cursor.forEach(function(doc) {
			    	var uid_tag_sender = doc['uid_tag_sender'];
			    	if (!(uid_tag_sender in array_messages))
			    		array_messages[uid_tag_sender] = doc['message_body'] + __.SEPARATOR_DATE + doc['message_date'];
			    	else 
			    		array_messages[uid_tag_sender] = array_messages[uid_tag_sender] + __.SEPARATOR_MESSAGES + doc['message_body'] + __.SEPARATOR_DATE + doc['message_date'];


			        // array_messages.push(doc['uid_tag_sender'] + '*' + doc['message_body']);
			        count = count + 1;
			    }, function(err) {

			    	if (err != null) {
			    		reject(err);
			    	}
			    	else {
			        	console.log(count + " messages from UID_Receiver '" + uid_receiver + "' has been found.");
			        	messages_collection.removeMany({uid_tag_receiver: uid_receiver}, function(err, r) 
						{
							if (err != null) {
								console.error(err);
								reject(err);
							}
							else if (count == 0)
								resolve(null);
							else
								resolve(array_messages);
						});
			        	
			    	}
			    });
			});	
		}
}

module.exports = DBMessagesManager;