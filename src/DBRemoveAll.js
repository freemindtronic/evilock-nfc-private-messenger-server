const MongoClient = require('mongodb').MongoClient
const test = require('assert');
const __ = require('./Constants.js');

MongoClient.connect(__.DB_URL, function(err, db) {
	test.equal(null, err);
	var collection = db.collection("messages");

	// Remove all the document
    collection.removeMany();

    // Fetch all results
    collection.find().toArray(function(err, items) {
      test.equal(null, err);
      test.equal(0, items.length);
      console.log("SUCCESS : Database has been cleaned from all entries.");
      db.close();
    });
});