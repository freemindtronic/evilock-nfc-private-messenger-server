const __ = require('./Constants.js');
var net = require('net');
var stdin = process.openStdin();

var isProtocolDefined = false;
var default_protocol = __.PROTOCOL_RECEIVE_MESSAGE;
var socket = new net.Socket();

socket.connect(7928, 'messaging.evitoken.com', function() {
	console.log('Connected to the server, please give protocol code (' + __.PROTOCOL_RECEIVE_MESSAGE + " / " +__.PROTOCOL_GET_USER_MESSAGES + " / " + __.PROTOCOL_EXIT + ')');
});
socket.setKeepAlive(true, "1000");

socket.on('data', function(data) {
    var dataFromServer = data.toString();
	
    switch (dataFromServer) 
    {
        case __.PROTOCOL_EXIT :
                socket.end();
                socket.destroy();
                break;
        default : 
                console.log('Server: ' + dataFromServer);
                break;
    }
});

socket.on('close', function() {
	console.log('Connection closed');
    process.exit(0);
});

stdin.addListener("data", function(d) {
    var input = d.toString().trim();

    if (isProtocolDefined)
    {   
        if (input == "def" && default_protocol == __.PROTOCOL_GET_USER_MESSAGES)
            socket.write("5f3f4a2f6a5c02e0"); //UID_RECEIVER
        else if (input == "def" && default_protocol == __.PROTOCOL_RECEIVE_MESSAGE)
            // socket.write("7939c1ed084402e0#5f3f4a2f6a5c02e0#dkdè*zd^fqfqqeeeff/éédzdkzfn#2017-10-12 16:04:05#" +  __.DEFAULT_TIMER_MINUTES); //UID_SENDER # UID_RECEIVER # MESSAGE_BODY # MESSAGE_DATE # TIMERMN
            socket.write("7939c1ed084402e0#5f3f4a2f6a5c02e0#4nZ0gF8X5aw1oZSKgybPFoCEKxWZQtBqLEzEbnzDAlw=;tlFrAaq5_ZuW_4HHe1sNsg==^2018/01/23 - 17:08:41#" +  __.DEFAULT_TIMER_MINUTES); //UID_SENDER # UID_RECEIVER # MESSAGE_BODY # MESSAGE_DATE # TIMERMN
        else
            socket.write(input);

        console.log("Data has been sent to the server...");
    }
    else if (input == __.PROTOCOL_RECEIVE_MESSAGE) 
    {
    	isProtocolDefined = true;
        console.log("Send message mode (write 'def' to send default message)... Use '#' character to concat the field like this\n\tUID_SENDER#UID_RECEIVER#MESSAGE_BODY%MESSAGE_DATE#TIMERMN");
    	socket.write(__.PROTOCOL_RECEIVE_MESSAGE);
    } else if (input == __.PROTOCOL_GET_USER_MESSAGES) 
    {
    	isProtocolDefined = true;
        default_protocol = __.PROTOCOL_GET_USER_MESSAGES;
        console.log("Get message mode (write 'def' to get message from default UID_RECEIVER)... Send the string corresponding to the UID_RECEIVER of the messages you want back");
    	socket.write(__.PROTOCOL_GET_USER_MESSAGES);
    } else 
        console.log("I don't know this command yet...");
 });