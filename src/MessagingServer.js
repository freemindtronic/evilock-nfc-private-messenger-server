// 'use strict';
//Import libraries
const DBMessagesManager = require('./DBMessagesManager.js');
const net = require('net');
const _ = require('lodash');
const periodic_task = require('node-schedule');
const MongoClient = require('mongodb').MongoClient;
const __ = require('./Constants.js');


//Connection with the database
var DB_Connection;
MongoClient.connect(__.DB_URL, function(err, db) {
	if (err != null)
		throw err;
	else
		DB_Connection = new DBMessagesManager(db);
});

//Definition of periodic task to be executed
var timerUpdateTask = periodic_task.scheduleJob('* * * * *', function() {  //Update of the timers every minute
	DB_Connection.updateTimers();
});


//Main definition of the TCP server
const server = net.createServer(function (socket) {

	//Definition of socket attributes
	socket.name = socket.remoteAddress + ":" + socket.remotePort;
	socket.authorize = false;
	try {
		socket.setKeepAlive(true, "5000");
	} catch(err) {
		console.error(err);
	}
	
	console.log('Client connected - ' + socket.name);

	//Set up of listeners
	socket.on('data', function(data) {

	    var dataReceived = data.toString().trim();

	    if (dataReceived == null || dataReceived.length == 0)
	    	return;
	    // else
	    // 	console.log("Data received from client : " + dataReceived);

	    if (socket.authorize == false) //First connection attempt
	    {	
	    	if (__.PROTOCOL_RECEIVE_MESSAGE == dataReceived || __.PROTOCOL_GET_USER_MESSAGES == dataReceived) //Receive protocol OK
	    	{
	    		socket.write(__.SERVER_ANSWER_RECEIVE_OK + '\n');
	    		socket.authorize = true;
	    		socket.request_type = dataReceived;
	    	} else {
	    		socket.end('ERROR');
	    		console.log("Authorization FAILED for client " + socket.name);
	    	}
	    } 
	    else if (socket.request_type == __.PROTOCOL_GET_USER_MESSAGES) //Someone is requesting his pending message and will now send his UID
	    {
	    	var promise = DB_Connection.getReceivedMessages(dataReceived);
	    	if (promise == -1)
	    		console.log("Error while reading the user (" + socket.name + ") messages");
	    	else {
	    		promise.then(function(resolve) 
	    		{
	    			if (resolve == null) {
	    				socket.end(__.PROTOCOL_NO_MESSAGES_FOUND);
	    			}
	    			else 
	    			{
	    				for (var messages in resolve) {
	    					socket.write(messages + __.SEPARATOR_UID_SENDER + resolve[messages]);
	    				}
	    				socket.end();
	    			}
	    		}, function(reject) {
	    			console.error(reject);
	    			socket.end('1');
	    		});
	    	}
	    } 
	    else if (socket.request_type == __.PROTOCOL_RECEIVE_MESSAGE)//Someone is sending a message to store on the server
	    {
	    	console.log('Received message from client : ' + dataReceived);
	    	var arrayMessageData = _.split(dataReceived, __.SEPARATOR_MESSAGES);
	    	
	    	if (arrayMessageData.length == 4) {
	    		DB_Connection.addMessage(arrayMessageData);
	    		socket.end('0');
	    	} else {
	    		socket.end('1');
	    		console.log("Error while parsing message data from " + socket.name);
	    	} 
	    }
	    else if (socket.request_type == __.PROTOCOL_EXIT) //We finish the socket process
	    {
	    	socket.end(__.PROTOCOL_EXIT);
	    	socket.destroy();
	    	console.log("The client " + socket.name + " has disconnected");
	    }
	});

	socket.on('error', function(err) {
  		console.error(err);
	}); 

	socket.on('end', function() {
		console.log('Client disconnected');
	});
});

server.on('error', function(err) {
  	console.error("Server error happened : " + err);
}); 

server.listen(__.SERVER_PORT, "0.0.0.0");
console.log("Server is listening on " + __.SERVER_PORT + " port...");