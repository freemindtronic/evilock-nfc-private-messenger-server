FROM node:latest

RUN mkdir /app
RUN mkdir -p /var/log/messaging-server
COPY src /app
WORKDIR /app

RUN npm install

CMD [ "npm", "start" ]
